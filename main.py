from flask import Flask, render_template, request
from faker import Faker
import requests

fake = Faker()

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello World"

@app.route('/requirements/')
def requirements():
    with open('requirements.txt') as file:
        lines = file.readlines()
    return render_template('requirements.html', items=lines)

@app.route('/generate-users/', methods=['GET', 'POST'])
def generate():

    count = request.values.get('counts', 100)
    users = [(fake.name(), fake.email()) for _ in range(count)]
    return render_template('fake.html', items=users)

@app.route('/space/')
def space():
    result = requests.get('http://api.open-notify.org/astros.json').json()['number']
    return render_template('space.html', result=result)

if __name__ == "__main__":
    app.run(debug=True, port=5002)
